# Infiny API Client

Infiny API Client - Laravel Package 

## Installation

Infiny API Client requires PHP 7.3-8.0. This particular version supports Laravel 8.

Edit Laravel's composer.json file to require the package and also indicate the repository in GitLab for Composer to know where to fetch packages aside from the default packagist repositories.

```
...
"require": {
    "php": "^7.3|^8.0",
    "fruitcake/laravel-cors": "^2.0",
    "guzzlehttp/guzzle": "^7.0.1",
    "laravel/framework": "^8.54",
    "laravel/sanctum": "^2.11",
    "laravel/tinker": "^2.5",
    "banovs/infinyapiclient": "^1.0.0"
},
...
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/dicho/infiny-api-client"
    }
]
...
```

Then run:

```
$ composer update
```

And this is it! You have the Infiny API Client package already installed in your Laravel project.

## Usage

Configure Infiny API Client credentials as environment variables in the Laravel project's .env file like:

```
INFINY_API_CLIENT_ID={client_id}
INFINY_API_CLIENT_SECRET={client_secret}
INFINY_API_ENDPOINT={endpoint} (API's base URL)
INFINY_API_VERSION={version} (e.g. INFINY_API_VERSION=1)

```

With Laravel's Service Container the requests can be just injected in a route, controller constructor or anywhere else:

```php
use Banovs\InfinyApiClient\Request\ServicesRequest;

...

Route::get('/request/services',
    function (ServicesRequest $servicesRequest) {
        return $servicesRequest->send();
    }
);

```

```php
use Banovs\InfinyApiClient\Request\ServiceDetailsRequest;

...

Route::get('/request/service/{serviceId}/details',
    function ($serviceId, ServiceDetailsRequest $serviceDetailsRequest) {
        return $serviceDetailsRequest->send($serviceId);
    }
);

```

```php
<?php

namespace App\Http\Controllers;

use Banovs\InfinyApiClient\Request\ServiceDetailsRequest;
use Banovs\InfinyApiClient\Request\ServicesRequest;
use Illuminate\Routing\Controller as BaseController;

class ServiceController extends BaseController
{
    /**
     * Services request
     *
     * @var ServicesRequest
     */
    protected $servicesRequest;

    /**
     * Service details request
     *
     * @var ServiceDetailsRequest
     */
    protected $serviceDetailsRequest;

    /**
     * Create a new controller instance.
     *
     * @param  ServicesRequest $servicesRequest
     * @param  ServiceDetailsRequest $serviceDetailsRequest
     */
    public function __construct(ServicesRequest $servicesRequest,
        ServiceDetailsRequest $serviceDetailsRequest)
    {
        $this->servicesRequest = $servicesRequest;
        $this->serviceDetailsRequest = $serviceDetailsRequest;
    }

    /**
     * Get all Infiny services
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function services()
    {
        $response = $this->servicesRequest->send();

        return $response;

    }

    /**
     * Get Infiny service details
     *
     * @param int $serviceId
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function serviceDetails($serviceId)
    {
        $response = $this->serviceDetailsRequest->send($serviceId);

        return $response;
    }
    
}
```

## Example

An example Laravel project with already installed Infiny API Client Package can be found [here](https://gitlab.com/dicho/laravel-infiny-api-client). 

## License

Infiny API Client is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).



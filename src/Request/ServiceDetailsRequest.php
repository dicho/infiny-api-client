<?php

namespace Banovs\InfinyApiClient\Request;

use Illuminate\Support\Facades\Http;

/**
 * Class ServiceDetailsRequest
 * @package Banovs\InfinyApiClient\Request
 *
 */
class ServiceDetailsRequest
{
    /**
     * Send request to Infiny API to get service details
     *
     * @param $serviceId
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function send($serviceId)
    {

        $authRequest = new AuthRequest();

        // Get API credentials from session
        $authCredentials = $authRequest->getSessionAuth();

        // If no credentials in the session or credentials expired
        // make authorisation request to the API
        if(!$authCredentials) {

            $authCredentials = $authRequest->send();

            // add datetime to obtained credentials
            // to easily check if expired within the session
            $authCredentials['created'] = new \DateTime();

            // store credentials in session
            session(['authCredentials' => $authCredentials]);
        }

        if (empty($authCredentials['access_token'])) {
            return response()->json([
                'Error' => 'Infiny API authorization failed! No access token!'
            ]);
        }

        // Request service details
        $response = Http::withHeaders([
            'Accept' => 'application/vnd.cloudlx.v' . env('INFINY_API_VERSION') . '+json',
            'Authorization' => 'Bearer ' . $authCredentials['access_token']
        ])->get(env('INFINY_API_ENDPOINT') . '/api/services/' . $serviceId . '/service');

        return $response->json();


    }
}

<?php

namespace Banovs\InfinyApiClient\Request;

use Illuminate\Support\Facades\Http;

/**
 * Class AuthRequest
 * @package Banovs\InfinyApiClient\Request
 *
 */
class AuthRequest
{
    /**
     * Send authorisation request to Infiny API
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function send()
    {

        $response = Http::withHeaders([
            'Accept' => 'application/vnd.cloudlx.v' . env('INFINY_API_VERSION') . '+json'
        ])->post(env('INFINY_API_ENDPOINT') . '/api/oauth2/access-token', [
            'grant_type' => 'client_credentials',
            'client_id' => env('INFINY_API_CLIENT_ID'),
            'client_secret' => env('INFINY_API_CLIENT_SECRET')
        ]);

        return $response->json();
    }

    /**
     * Get authorization credentials from session
     * if authorization request sent already
     *
     * @return \Illuminate\Session\Store|mixed|null
     */
    public function getSessionAuth()
    {
        $authCredentials = NULL;

        if(session('authCredentials')) {
            $curDateTime = new \DateTime();
            $sesDateTime = session('authCredentials')['created'];
            $sesLife = $curDateTime->getTimestamp() - $sesDateTime->getTimestamp(); //seconds

            // Check if session credentials expired
            if($sesLife < session('authCredentials')['expires_in']) {
                $authCredentials = session('authCredentials') ;
            }
        }

        return $authCredentials;
    }
}

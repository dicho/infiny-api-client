<?php

namespace Banovs\InfinyApiClient\Request;

use Illuminate\Support\Facades\Http;

/**
 * Class ServicesRequest
 * @package Banovs\InfinyApiClient\Request
 *
 */
class ServicesRequest
{
    /**
     * Send request to Infiny API to get all services
     *
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function send()
    {

        $authRequest = new AuthRequest();

        // Get API credentials from session
        $authCredentials = $authRequest->getSessionAuth();

        // If no credentials in the session or credentials expired
        // make authorisation request to the API
        if(!$authCredentials) {

            $authCredentials = $authRequest->send();

            // add datetime to obtained credentials
            // to easily check if expired within the session
            $authCredentials['created'] = new \DateTime();

            // store credentials in session
            session(['authCredentials' => $authCredentials]);
        }

        if (empty($authCredentials['access_token'])) {
            return response()->json([
                'Error' => 'Infiny API authorization failed! No access token!'
            ]);
        }

        // Request services
        $response = Http::withHeaders([
            'Accept' => 'application/vnd.cloudlx.v' . env('INFINY_API_VERSION') . '+json',
            'Authorization' => 'Bearer ' . $authCredentials['access_token']
        ])->get(env('INFINY_API_ENDPOINT') . '/api/services');

        return $response->json();


    }
}
